require 'fileutils'
require 'capybara/rspec'
require 'capybara/cucumber'
require 'capybara'
require 'rspec'
require 'cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'pry'

class Login < SitePrism::Page

    element :buttonLogin, :xpath , "//a[@class = 'btn dropdown-toggle hidden-sm hidden-xs']"
    element :campoCPF, :xpath , "(//input[@class= 'form-control cpf'])[1]"
    element :campoSenha, :xpath , "//input[@name= 'senha']"
    element :buttonEfetuarLogin, :xpath , "//button[@id= 'login-button']"



    def validarHomeSite()
        find(:xpath, " //img[@class= 'img-responsive center-block logo']")
        find(:xpath, "//div[@class= 'cd-dropdown-wrapper']")
        find(:xpath, " //div[@class= 'logos-group']")   
    end

    def efetuaLogin()
        buttonLogin.click
        campoCPF.click
        campoCPF.set("45014340822")
        campoSenha.set("123321")
        buttonEfetuarLogin.click
    end

    def validaLogin()
        find(:xpath, "//a[@class= 'btn dropdown-toggle logado hidden-sm hidden-xs']")
    end

    def validaResultado()
        if  has_xpath?("//a[@class= 'btn dropdown-toggle logado hidden-sm hidden-xs']", wait: 1)
            find(:xpath, "//a[@class= 'btn dropdown-toggle logado hidden-sm hidden-xs']")
        else
            puts ("Não foi possivel validar o Login")
        end
    end
end