Dado(/^que eu esteja na home do site movida$/) do
    visit "https://www.movida.com.br"
    
    Login.new.validarHomeSite()
    
end

Quando(/^eu efetuar login$/) do
    Login.new.efetuaLogin()
end

Então(/^o site apresenta a mensagem de boas vindas no canto superior direito$/) do
    Login.new.validaLogin()
end

Então(/^o sistema apresenta a mensagem de boas vindas ou apresenta uma mensagem de erro$/) do
    Login.new.validaResultado()
end