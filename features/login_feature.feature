#language: pt
#utf-8

Funcionalidade: Efetuar Login
    Eu como usuario do site 
    Quero efetuar login no site
    Para utilizar seus recursos

Contexto: Cliente Acessa a home do site Movida e efetua Login
    Dado que eu esteja na home do site movida
    Quando eu efetuar login

@testeMovida
Cenario: 01 - Efetua Login
    Então o site apresenta a mensagem de boas vindas no canto superior direito

@testeMovida
Cenario: 02 - Efetua Login - Positivo Negativo
    Então o sistema apresenta a mensagem de boas vindas ou apresenta uma mensagem de erro